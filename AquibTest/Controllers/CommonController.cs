﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AquibTest.DAO;
using AquibTest.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AquibTest.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly OtpVerificationDAO otpVerificationDAO;

        public CommonController(OtpVerificationDAO otpVerificationDAO)
        {
            this.otpVerificationDAO = otpVerificationDAO;
        }

        [HttpGet("{mobile}")]

        public IActionResult GenerateOtp(string mobile)
        {
            var otp = otpVerificationDAO.GenerateOtp(mobile);
            return Ok(otp);
        }

        [HttpPost]

        public IActionResult VerifyOtp(OtpVerification objOtpData)
        {
            var verification = otpVerificationDAO.VerifyOtp(objOtpData.Mobile,objOtpData.Otp);
            return Ok(verification);
        }
    }
}
