﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AquibTest.DAO
{
    public interface OtpVerificationDAO
    {
       string GenerateOtp(string mobile);
       string VerifyOtp(string mobile, int otp);
    }
}
