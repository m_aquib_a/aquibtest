﻿using AquibTest.DAO;
using AquibTest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AquibTest.Repository
{
    public class OtpVerificationRepository : OtpVerificationDAO
    {
        private readonly AppDbContext db;

        public OtpVerificationRepository(AppDbContext db)
        {
            this.db = db;
        }
        public string GenerateOtp(string mobile)
        {
            Random r = new Random();
            int randNum = r.Next(1000000);
            var otp = randNum.ToString("D6");
            if (SaveOtp(mobile, int.Parse(otp)))
                return otp;
            else
                return "";
        }

        public bool SaveOtp(string mobile, int otp)
        {
            bool alreadyExist = db.OV_Otp_Verification.Any(z => z.Mobile.Equals(mobile));

            if(alreadyExist)
            {
                var existingRecords = db.OV_Otp_Verification.Where(z => z.Mobile.Equals(mobile)).FirstOrDefault();
                db.OV_Otp_Verification.Remove(existingRecords);
                db.SaveChanges();
            }

            OtpVerification objOtpVerification = new OtpVerification();

            objOtpVerification.Mobile = mobile;
            objOtpVerification.Otp = otp;
            objOtpVerification.GeneratedOn = DateTime.Now;

            db.OV_Otp_Verification.Add(objOtpVerification);
            try
            {
                db.SaveChanges();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
            
        }

        public string VerifyOtp(string mobile, int otp)
        {
            var record = db.OV_Otp_Verification.Where(z => z.Mobile==(mobile)).FirstOrDefault();
            
            if (record.Otp == otp)
            {
                return "Valid";
            }
            else
            {
                return "Inavlid";
            }
        }
    }
}
