﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AquibTest.Model
{
    public class OtpVerification
    {
        public int Id { get; set; }
        public string Mobile { get; set; }
        public int Otp { get; set; }
        public DateTime? GeneratedOn { get; set; }
    }


}
