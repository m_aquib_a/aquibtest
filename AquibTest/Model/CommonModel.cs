﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AquibTest.Model
{
    public class CommonModel
    {
        public class OtpVerification
        {
            public string Mobile { get; set; }
            public int Otp { get; set; }

        }
    }
}
